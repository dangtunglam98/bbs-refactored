package models
import collection.mutable

object BlogPostInMemory {
  private val users = mutable.Map[String, String]("Lam" -> "Pyone")
  private var blogPost = List[(String, String)](("Lam", "Hello World"), ("Septeni", "Goodbye" ))
  def validateUser(username: String, password: String): Boolean = {
    users.get(username).map(_ == password).getOrElse(false)
  }


  
  def createUser(username: String, password: String) : Boolean = {
    if (users.contains(username)) {
      false
    } else {
      users(username) = password
      true
    }
  }

  def getBlogs : List[(String, String)] = {
    blogPost
  }

  def addBlog(username: String, post: String) : Unit = {
    blogPost = (username, post) :: blogPost
  }

}
