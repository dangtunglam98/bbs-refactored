name := """bbs_blog"""
organization := "septeni"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

herokuAppName in Compile := "stormy-oasis-14810"

scalaVersion := "2.13.3"

libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test

// https://mvnrepository.com/artifact/com.typesafe.play/play-json
libraryDependencies += "com.typesafe.play" %% "play-json" % "2.9.0"
libraryDependencies += "org.postgresql" % "postgresql" % "42.2.14"
libraryDependencies += "com.typesafe.slick" %% "slick-codegen" % "3.3.2"
libraryDependencies += "com.typesafe.play" %% "play-slick" % "5.0.0"

libraryDependencies += "com.typesafe.slick" %% "slick-hikaricp" % "3.3.2"


// Adds additional packages into Twirl
//TwirlKeys.templateImports += "septeni.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "septeni.binders._"


