import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneServerPerSuite
import org.scalatestplus.play.OneBrowserPerTest
import org.scalatestplus.play.OneBrowserPerSuite
import org.scalatestplus.play.HtmlUnitFactory

class UserInterfaceSpec extends PlaySpec with GuiceOneServerPerSuite with OneBrowserPerSuite with HtmlUnitFactory {
    "User Interface" must {
        "login and access function" in {
            go to s"http://localhost:$port/load"
            click on "loginName"
            emailField("loginName").value = "lam@septeni.com"
            click on "loginPass"
            pwdField("loginPass").value = "123"
            click on "loginSubmit"

        }

        "create user and access function" in {
            go to s"http://localhost:$port/load"
            click on "newName"
            emailField("newName").value = "lam@septeni.com"
            click on "newPass"
            pwdField("newPass").value = "123"
            click on "createSubmit"
        }
    }
}